import React from 'react';
import { Route, Link } from 'react-router-dom';

import image from '../images/image';
import pngimage from '../images/pngfile';

class HomeComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="home">
                <h1>Home Component</h1>
                <p>Font Awesomeasdasdasdasd<i className="fa fa-address-card"></i>!!!</p>
                <div>
                <img className="pngfile" src={pngimage} alt=""/>
                </div>
            </div>
        )
    }
}

export default HomeComponent;