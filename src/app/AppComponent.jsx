import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import asyncComponent from '../asyncComponent';

import HeaderComponent from '../header/HeaderComponent';
import HomeComponent from '../home/HomeComponent';
import AboutComponent from '../about/AboutComponent';
import Styles from '../style/main';

class AppComponent extends React.Component {
    
    render() {
        return (
            <BrowserRouter>
                <div>
                    <HeaderComponent />
                    <Switch>
                        <Route exact path='/' component={HomeComponent} />
+                       <Route path='/about' component={AboutComponent} />
                        <Route render={() => <h1>Page not found</h1>}></Route>
                    </Switch>
                </div>
            </BrowserRouter>
        )
    }
}

export default AppComponent;