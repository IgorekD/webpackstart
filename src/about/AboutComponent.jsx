import React from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';

import AboutSubComponent from './AboutSubComponent';

class AboutComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="about">
                <h1>About Page</h1>
                <Link to='/about/sub'>SubComponent</Link>
                <Route path='/about/sub' component={AboutSubComponent} />
            </div>
        )
    }
}

export default AboutComponent;