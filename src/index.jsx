import React from 'react';
import ReactDOM from 'react-dom';

import { AppContainer } from 'react-hot-loader';
import AppComponent from './app/AppComponent';

const render = (Component) => {
    const contentRoot = document.getElementById('app')
    ReactDOM.render(
        <AppContainer>
            <Component/>
        </AppContainer>
    , contentRoot);
}

render(AppComponent);

if(module.hot){
    module.hot.accept('./app/AppComponent', () => {
        render(AppComponent);
    });
}